#!/usr/bin/env bash
git init

git submodule add https://github.com/yudai/gotty.git source/gotty

git submodule add https://github.com/github-38438-org/xterm.js.git source/xterm.js

git submodule add https://github.com/github-38438-org/webtty.git source/webtty


git submodule add https://github.com/github-38438-org/ttyd.git source/ttyd

git submodule update

git submodule sync